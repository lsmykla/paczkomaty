<?php
/**
 * Copyright © 2018 Madev. All rights reserved.
 */
namespace Madev\Paczkomaty\Block\System\Config\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

/**
 * Class Methods Backend system config array field renderer
 */
class Methods extends AbstractFieldArray
{
    /**
     * Initialise columns for 'Methods'
     * Label is name of field
     * Class is storefront validation action for field
     *
     * @return void
     */
    protected function _construct()
    {
        $this->addColumn(
            'title',
            [
                'label' => __('Title'),
                'class' => 'validate-no-empty validate-alphanum-with-spaces'
            ]
        );
        $this->addColumn(
            'message',
            [
                'label' => __('Message'),
                'class' => 'validate-no-empty'
            ]
        );
        $this->addColumn(
            'price',
            [
                'label' => __('Price'),
                'class' => 'validate-no-empty'
            ]
        );
        $this->_addAfter = false;
        parent::_construct();
    }
}
