<?php
/**
 * Copyright © 2018 Madev. All rights reserved.
 */

namespace Madev\Paczkomaty\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Config;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\Method;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Psr\Log\LoggerInterface;

/**
 * Class Carrier Paczkomaty shipping model
 */
class Carrier extends AbstractCarrier implements CarrierInterface
{
    /**
     * Carrier's code
     *
     * @var string
     */
    protected $_code = 'paczkomaty';

    /**
     * Whether this carrier has fixed rates calculation
     *
     * @var bool
     */
    protected $_isFixed = true;

    /**
     * @var ResultFactory
     */
    protected $rateResultFactory;

    /**
     * @var MethodFactory
     */
    protected $rateMethodFactory;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param ResultFactory $rateResultFactory
     * @param MethodFactory $rateMethodFactory
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        array $data = []
    )
    {
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * Generates list of allowed carrier`s shipping methods
     * Displays on cart price rules page
     *
     * @return array
     * @api
     */
    public function getAllowedMethods()
    {
        return [$this->getCarrierCode() => __($this->getConfigData('name'))];
    }

    /**
     * Collect and get rates for storefront
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param RateRequest $request
     * @return DataObject|bool|null
     * @api
     */
    public function collectRates(RateRequest $request)
    {
        /**
         * Make sure that Shipping method (Paczkomaty) is enabled
         */
        if (!$this->isActive()) {
            return false;
        }

        /**
         * Build Rate for each method
         * Each Rate displayed as shipping method under Carrier(Paczkomaty) on frontend
         */
        $result = $this->rateResultFactory->create();
        foreach ($this->getMethods() as $methodId => $methodData) {
            $method = $this->buildRateForMethod($methodId, $methodData);
            $result->append($method);
        }

        return $result;
    }

    /**
     * Build Rate based on method data
     *
     * @param $methodId
     * @param array $methodData
     * @return Method
     */
    protected function buildRateForMethod($methodId, array $methodData)
    {
        $rateResultMethod = $this->rateMethodFactory->create();
        /**
         * Set carrier's method data
         */
        $rateResultMethod->setData('carrier', $this->getCarrierCode());
        $rateResultMethod->setData('carrier_title', $this->getConfigData('title'));

        /**
         * Displayed as shipping method under Paczkomaty carrier
         */
        $methodTitle = sprintf(
            '%s, %s',
            $methodData['title'],
            $methodData['message']
        );
        $rateResultMethod->setData('method_title', $methodTitle);
        $rateResultMethod->setData('method', $methodId);

        $rateResultMethod->setPrice($methodData['price']);
        $rateResultMethod->setData('cost', $methodData['price']);

        return $rateResultMethod;
    }

    /**
     * Get methods info
     *
     * @return array
     */
    protected function getMethods()
    {
        $methods = [];
        $configData = $this->getConfigData('methods');
        if (is_string($configData) && !empty($configData)) {
            $methods = json_decode($configData, true);
        }

        $result = [];
        foreach ($methods as $method) {
            $methodId = strtolower(str_replace(' ', '_', $method['title'])) . '_' . strtolower(str_replace(' ', '_', $method['message']));
            $result[$methodId] = [
                'title' => $method['title'],
                'message' => $method['message'],
                'price' => $method['price'],
            ];
        }

        return $result;
    }
}

