<?php
/**
 * Copyright © 2018 Madev. All rights reserved.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Madev_Paczkomaty',
    __DIR__
);
