<?php
/***
 * Copyright © 2018 Madev. All rights reserved.
 */
namespace Madev\Paczkomaty\Test\Unit\Block\System\Config\Form\Field;

use Madev\Paczkomaty\Block\System\Config\Form\Field\Methods;

/**
 * Class MethodsTest
 */
class MethodsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Methods
     */
    protected $block;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        /** @var \Magento\Backend\Block\Template\Context $context */
        $context = $this->getMockBuilder('Magento\Backend\Block\Template\Context')
            ->disableOriginalConstructor()
            ->getMock();

        $this->block = new Methods($context);
    }

    public function testGetColumns()
    {
        $this->assertArrayHasKey('title', $this->block->getColumns());
        $this->assertArrayHasKey('price', $this->block->getColumns());
        $this->assertArrayHasKey('message', $this->block->getColumns());
        $this->assertCount(3, $this->block->getColumns());
    }
}
